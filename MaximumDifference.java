//{ Driver Code Starts
import java.util.*;

class GFG
{
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t>0)
		{
			int n = sc.nextInt();
			int a[] = new int[n];
			for(int i=0; i<n; i++)
				a[i] = sc.nextInt();
			Solution ob = new Solution();
			System.out.println(ob.findMaxDiff(a,n));
		t--;
		}
	}
}
// } Driver Code Ends


class Solution {
    int findMaxDiff(int a[], int n) {
        // Arrays to store nearest smaller elements on left and right
        int LS[] = new int[n];
        int RS[] = new int[n];

        // Populate LS using a stack
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            while (!stack.isEmpty() && a[stack.peek()] >= a[i]) {
                stack.pop();
            }
            LS[i] = stack.isEmpty() ? 0 : a[stack.peek()];
            stack.push(i);
        }

        // Populate RS using a stack on the reversed array
        stack.clear();
        for (int i = n - 1; i >= 0; i--) {
            while (!stack.isEmpty() && a[stack.peek()] >= a[i]) {
                stack.pop();
            }
            RS[i] = stack.isEmpty() ? 0 : a[stack.peek()];
            stack.push(i);
        }

        // Find the maximum absolute difference between LS and RS
        int maxDiff = -1;
        for (int i = 0; i < n; i++) {
            maxDiff = Math.max(maxDiff, Math.abs(LS[i] - RS[i]));
        }

        return maxDiff;
    }
}
